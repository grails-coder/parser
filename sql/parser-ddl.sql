
CREATE DATABASE /*IF NOT EXISTS*/`parser`;

USE `parser`;

DROP TABLE IF EXISTS `blockedip`;

CREATE TABLE `blockedip` (
  `entryId` bigint(20) NOT NULL,
  `createdOn` tinyblob,
  `ipAddress` varchar(255) DEFAULT NULL,
  `reason` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`entryId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

DROP TABLE IF EXISTS `recordentry`;

CREATE TABLE `recordentry` (
  `entryId` bigint(20) NOT NULL,
  `date` datetime DEFAULT NULL,
  `httpOperation` varchar(255) DEFAULT NULL,
  `httpResponseCode` int(11) DEFAULT NULL,
  `httpUserAgent` varchar(255) DEFAULT NULL,
  `ipAddress` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`entryId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
