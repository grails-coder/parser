package com.ef.repository;

import com.ef.domain.RecordEntry;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import java.util.Date;
import java.util.List;

/**
 * @author ArmandodeJesus
 * @email aj.montoya [ at ] outlook.com
 * @Date 10/7/2017
 * @Copyright Armando Montoya, 2017
 * All rights reserved
 */
public interface RecordEntryRepository extends CrudRepository<RecordEntry, Long> {

    @Query(value = "SELECT *, count(*) as threshold from recordentry " +
            "WHERE date BETWEEN ?1 AND ?2 GROUP BY ipAddress HAVING threshold > ?3",
            nativeQuery = true)
    public List<RecordEntry> findAllByDateAndThreshold(Date startDate, Date endDate, Integer threshold);

    public List<RecordEntry> findAllByDate(String date);

}
