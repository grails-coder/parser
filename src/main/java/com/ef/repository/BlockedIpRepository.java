package com.ef.repository;

import com.ef.domain.BlockedIp;
import org.springframework.data.repository.CrudRepository;

/**
 * @author ArmandodeJesus
 * @email aj.montoya [ at ] outlook.com
 * @Date 10/8/2017
 * @Copyright Armando Montoya, 2017
 * All rights reserved
 */
public interface BlockedIpRepository extends CrudRepository<BlockedIp, Long> {
}
