package com.ef.util;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.Date;

/**
 * @author ArmandodeJesus
 * @email aj.montoya [ at ] outlook.com
 * @Date 10/9/2017
 * @Copyright Armando Montoya, 2017
 * All rights reserved
 */
public class Common {

    public static final String CLI_DATE_PATTERN = "yyyy-MM-dd.HH:mm:ss";
    public static final String FILE_DATE_PATTERN = "yyyy-MM-dd HH:mm:ss.SSS";

    public static Date cliStringToDate(String inputDateTime){
        LocalDateTime dateTime = stringToLocalDateTime(inputDateTime, CLI_DATE_PATTERN);
        return Date.from(dateTime.atZone(ZoneId.systemDefault()).toInstant());
    }

    public static Date fileStringToDate(String inputDateTime){
        LocalDateTime dateTime = stringToLocalDateTime(inputDateTime, FILE_DATE_PATTERN);
        return Date.from(dateTime.atZone(ZoneId.systemDefault()).toInstant());
    }

    public static Date increaseCliDate(String inputDateTime, String increase){
        LocalDateTime dateTime = stringToLocalDateTime(inputDateTime, CLI_DATE_PATTERN);
        switch (increase){
            case "hourly":
                dateTime = dateTime.plusHours(1L);
                break;
            case "daily":
                dateTime = dateTime.plusHours(24L);
                break;
        }
        return Date.from(dateTime.atZone(ZoneId.systemDefault()).toInstant());
    }

    public static LocalDateTime stringToLocalDateTime(String inputDateTime, String datePattern){
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern(datePattern);
        return LocalDateTime.parse(inputDateTime, formatter);
    }

}
