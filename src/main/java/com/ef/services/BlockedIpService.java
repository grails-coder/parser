package com.ef.services;

import com.ef.domain.BlockedIp;
import com.ef.repository.BlockedIpRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.sql.DataSource;
import java.util.List;

/**
 * @author ArmandodeJesus
 * @email aj.montoya [ at ] outlook.com
 * @Date 10/8/2017
 * @Copyright Armando Montoya, 2017
 * All rights reserved
 */
@Service
public class BlockedIpService {

    @Autowired
    private BlockedIpRepository repository;

    @Autowired
    private DataSource dataSource;

    public Iterable<BlockedIp> saveList(List<BlockedIp> itemList){
        return repository.save(itemList);
    }

}
