package com.ef.services;

import com.ef.domain.RecordEntry;
import com.ef.repository.RecordEntryRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.sql.DataSource;
import java.util.Date;
import java.util.List;
import static com.ef.util.Common.*;

/**
 * @author ArmandodeJesus
 * @email aj.montoya [ at ] outlook.com
 * @Date 10/7/2017
 * @Copyright Armando Montoya, 2017
 * All rights reserved
 */
@Service
public class RecordEntryService {

    @Autowired
    private RecordEntryRepository repository;

    @Autowired
    private DataSource dataSource;

    @Transactional
    public Iterable<RecordEntry> saveList(List<RecordEntry> record) {
        return repository.save(record);
    }

    @Transactional
    public List<RecordEntry> findAllByDateAndDurationAndThreshold(String date, String duration, Integer threshold) {
        // since JPA version does not natively support Java 8 LocalDateTime, following helpers were created
        // for tricking it the easiest possible
        System.out.println("Looking up log records in database");

        Date startDate = cliStringToDate(date);
        Date endDate = increaseCliDate(date, duration);

        System.out.println("Using parameters: [startDate:" + startDate.toString()
                + "] [endDate:" + endDate.toString()
                + "] [threshold: " + threshold +"]");
        return repository.findAllByDateAndThreshold(startDate, endDate, threshold);
    }

}
