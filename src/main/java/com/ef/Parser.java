package com.ef;

import com.ef.config.AppConfig;
import com.ef.domain.BlockedIp;
import com.ef.domain.RecordEntry;
import com.ef.services.BlockedIpService;
import com.ef.services.RecordEntryService;
import org.apache.commons.cli.*;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.transaction.annotation.Transactional;

import java.io.File;
import java.io.FileNotFoundException;
import java.time.format.DateTimeParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Scanner;
import java.util.stream.Collectors;

import static com.ef.util.Common.*;

/**
 * @author ArmandodeJesus
 * @email aj.montoya [ at ] outlook.com
 * @Date 10/7/2017
 * @Copyright Armando Montoya, 2017
 * All rights reserved
 */
@Configuration("mainBean")
@EnableJpaRepositories(basePackages = {"com.ef.repository"})
@ComponentScan(basePackages = {"com.ef"})
@Import(AppConfig.class)
@Transactional
public class Parser {

    private Boolean validCliArguments = false;
    private CommandLine line;

    public Boolean areValidCliArguments() {
        validCliArguments = (line.hasOption("duration") && line.hasOption("startDate") && line.hasOption("threshold"));

        switch (line.getOptionValue("duration")){
            case "hourly":
            case "daily":
                validCliArguments = true;
                break;
            default:
                System.err.println("Parameter duration, value not allowed.");
                return false;
        }

        try {
            // if no exception arises then passed CLI date is OK
            stringToLocalDateTime(this.line.getOptionValue("startDate"), CLI_DATE_PATTERN);
            validCliArguments = true;
        } catch (DateTimeParseException ex){
            System.err.println("Parameter startDate does not meet specified format " + CLI_DATE_PATTERN);
            return false;
        }

        try{
            Integer.parseInt(line.getOptionValue("threshold"));
        } catch (NumberFormatException ex){
            System.err.println("Parameter threshold is not an integer.");
            return false;
        }

        return validCliArguments;
    }

    public CommandLine getLine() {
        return line;
    }

    public void setLine(CommandLine line) {
        this.line = line;
    }

    public Options createArgOptions(){
        Options result = new Options();
        result.addOption(Option.builder().longOpt("startDate").hasArg().valueSeparator('=').desc("Start date in format: yyyy-MM-dd.HH:mm:ss")
                .required(true).build());
        result.addOption(Option.builder().longOpt("duration").hasArg().required(true).valueSeparator('=')
                .desc("Establishes the time window after start date, only 'hourly' or 'daily' are accepted.").build());
        result.addOption(Option.builder().longOpt("threshold").hasArg().valueSeparator('=')
                .desc("Sets the threshold to look for and to block")
                .required(true).build());
        result.addOption(Option.builder().longOpt("accesslog").hasArg().required(false).valueSeparator('=')
                .desc("(Optional) Points to a valid .csv or .txt file").build());
        return result;
    }

    public CommandLine parseArgs(String[] args){

        // create the parser
        CommandLineParser parser = new DefaultParser();
        try {
            // parse the command line arguments
            this.line = parser.parse( this.createArgOptions(), args );
            return line;
        }
        catch( ParseException exp ) {
            System.err.println( "Parsing failed.  Reason: " + exp.getMessage() );
            this.displayUsage();
            System.exit(1);
        }
        return null;
    }

    public List<RecordEntry> loadFileContent(String filePath){
        System.out.println("Loading file: " + filePath);
        List<RecordEntry> result = new ArrayList<>();

        try {
            // open file
            Scanner scanner = new Scanner(new File(filePath));

            while (scanner.hasNextLine()){
                String inputLine = scanner.nextLine();

                // each line contains:
                // date | ip_address | http_operation | http_response_code | http_user_agent
                String[] lineValues = inputLine.split("\\|");

                RecordEntry recordEntry = new RecordEntry();
                recordEntry.setDate(fileStringToDate(lineValues[0]));
                recordEntry.setIpAddress(lineValues[1]);
                recordEntry.setHttpOperation(lineValues[2]);
                recordEntry.setHttpResponseCode(Integer.parseInt(lineValues[3]));
                recordEntry.setHttpUserAgent(lineValues[4]);

                result.add(recordEntry);

            }

        } catch (FileNotFoundException e) {
            System.err.println("File not found: " + filePath + ". Nothing to be processed");
            //e.printStackTrace();
            return result;
        }

        return result;
    }

    public Boolean processFile(AnnotationConfigApplicationContext ctx){
        Boolean success = false;
        List<RecordEntry> entries = this.loadFileContent(this.line.getOptionValue("accesslog"));
        if(0 != entries.size()){
            success = this.saveToDB(entries, ctx);
        }

        return success;
    }

    private Boolean saveToDB(List<RecordEntry> recordEntryList, AnnotationConfigApplicationContext ctx){
        Boolean result = false;

        RecordEntryService recordEntryService = ctx.getBean(RecordEntryService.class);
        if( 0 != recordEntryList.size()) {
            if (null != recordEntryService.saveList(recordEntryList)) {
                result = true;
                System.out.println("Saved log records");
            } else {
                result = false;
                System.err.println("Error when saving log records ");
            }
        } else {
            System.out.println("No log entries to be saved");
            result = true;
        }
        return result;
    }

    public List<RecordEntry> issueQuery(AnnotationConfigApplicationContext ctx){
        RecordEntryService recordEntryService = ctx.getBean(RecordEntryService.class);
        return recordEntryService.findAllByDateAndDurationAndThreshold(
                this.line.getOptionValue("startDate"),
                this.line.getOptionValue("duration"),
                Integer.parseInt(this.line.getOptionValue("threshold")));
    }

    public void saveBlockedIps(List<RecordEntry> recordEntryList, AnnotationConfigApplicationContext ctx) {
        BlockedIpService blockedIpService = ctx.getBean(BlockedIpService.class);
        if(0 != recordEntryList.size()){

            // map the RecordEntry List to a BlockedIp List
            List<BlockedIp> blockedIpList = recordEntryList.stream().map( recordEntry -> {
                BlockedIp blockedIp = new BlockedIp();
                blockedIp.setIpAddress(recordEntry.getIpAddress());
                blockedIp.setReason("IP blocked because exceeded criteria, " +
                        "threshold: " + this.line.getOptionValue("threshold") + " " +
                        "duration: " + this.line.getOptionValue("duration") + " " +
                        "startDate: " + this.line.getOptionValue("startDate") + " "
                );
                blockedIp.setCreatedOn(new Date());
                return blockedIp;
            }
            ).collect(Collectors.toList());

            //proceed to save blocked ip list
            if(null != blockedIpService.saveList(blockedIpList)){
                System.out.println("Saved blocked IP List");
            } else {
                System.err.println("Error when saving blocked IP's");
            }

        } else {
            System.out.println("No IP's to be blocked");
        }
    }

    public void displayResults(List<RecordEntry> list){
        System.out.println("---------------- DISPLAYING RESULTS (blocked IP's) --------------------");
        for (RecordEntry item:list) {
            System.out.println(item.getIpAddress());
        }
        System.out.println("------------------------- END OF RESULTS ---------------------------");
    }

    public void displayUsage(){
        HelpFormatter formatter = new HelpFormatter();
        formatter.printHelp( "java -cp parser.jar com.ef.Parser", this.createArgOptions());
    }

    public static void main(String[] args){

        AnnotationConfigApplicationContext applicationContext = new AnnotationConfigApplicationContext();

        try {

            applicationContext.register(Parser.class);
            applicationContext.refresh();

            Parser parser = (Parser) applicationContext.getBean("mainBean");
            parser.parseArgs(args);

            // validate CLI parameters
            if (parser.areValidCliArguments()){

                // if parameter accesslog is present then load the text file into the DB
                if (parser.getLine().hasOption("accesslog")){

                    // process the incoming file
                    if (parser.processFile(applicationContext)) {
                        System.out.println("File processed correctly ");
                    } else {
                        System.err.println("Failure when attempting to process input file: " +
                                parser.getLine().getOptionValue("accesslog"));
                    }

                }

                // issue the query
                List<RecordEntry> queryResult = parser.issueQuery(applicationContext);
                if (0 != queryResult.size()) {

                    // display query results
                    parser.displayResults(queryResult);

                    // save information into the blockedip table
                    parser.saveBlockedIps(queryResult, applicationContext);

                } else {
                    System.out.println("No results to show");
                }

            } else {
                //terminate the program since no valid options were provided
                parser.displayUsage();
                System.exit(1);
            }

        }
        finally {
            applicationContext.close();
        }

    }

}
