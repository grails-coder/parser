package com.ef.domain;

import javax.persistence.*;
import java.util.Date;

/**
 * @author ArmandodeJesus
 * @email aj.montoya [ at ] outlook.com
 * @Date 10/7/2017
 * @Copyright Armando Montoya, 2017
 * All rights reserved
 */

@Entity
//@Table(name = "record_entry")
public class RecordEntry {

    @Id
    @GeneratedValue(strategy = GenerationType.TABLE)
    private Long entryId;
    private Date date;
    private String ipAddress;
    private String httpOperation;
    private Integer httpResponseCode;
    private String httpUserAgent;

    public Long getEntryId() {
        return entryId;
    }

    public void setEntryId(Long entryId) {
        this.entryId = entryId;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getIpAddress() {
        return ipAddress;
    }

    public void setIpAddress(String ipAddress) {
        this.ipAddress = ipAddress;
    }

    public String getHttpOperation() {
        return httpOperation;
    }

    public void setHttpOperation(String httpOperation) {
        this.httpOperation = httpOperation;
    }

    public Integer getHttpResponseCode() {
        return httpResponseCode;
    }

    public void setHttpResponseCode(Integer httpResponseCode) {
        this.httpResponseCode = httpResponseCode;
    }

    public String getHttpUserAgent() {
        return httpUserAgent;
    }

    public void setHttpUserAgent(String httpUserAgent) {
        this.httpUserAgent = httpUserAgent;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        RecordEntry recordEntry = (RecordEntry) o;

        if (!entryId.equals(recordEntry.entryId)) return false;
        if (!date.equals(recordEntry.date)) return false;
        if (!ipAddress.equals(recordEntry.ipAddress)) return false;
        if (!httpOperation.equals(recordEntry.httpOperation)) return false;
        if (!httpResponseCode.equals(recordEntry.httpResponseCode)) return false;
        return httpUserAgent.equals(recordEntry.httpUserAgent);
    }

    @Override
    public int hashCode() {
        int result = entryId.hashCode();
        result = 31 * result + date.hashCode();
        result = 31 * result + ipAddress.hashCode();
        result = 31 * result + httpOperation.hashCode();
        result = 31 * result + httpResponseCode.hashCode();
        result = 31 * result + httpUserAgent.hashCode();
        return result;
    }

    @Override
    public String toString() {
        return new StringBuilder()
                .append("Log entry: ")
                .append(this.entryId)
                .append(" ")
                .append(this.date)
                .append(" ")
                .append(this.ipAddress)
                .toString();
    }

    public String toLongString() {
        return new StringBuilder()
                .append("Log entry: ")
                .append(this.ipAddress)
                .append("\t\t")
                .append(this.date)
                .append("\t")
                .append(this.httpOperation)
                .append("\t")
                .append(this.httpResponseCode)
                .append("\t")
                .append(this.httpUserAgent)
                .toString();
    }
}
