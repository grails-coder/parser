package com.ef.domain;

import javax.persistence.*;
import java.util.Date;

/**
 * @author ArmandodeJesus
 * @email aj.montoya [ at ] outlook.com
 * @Date 10/8/2017
 * @Copyright Armando Montoya, 2017
 * All rights reserved
 */
@Entity
//@Table(name = "blocked_ip")
public class BlockedIp {

    @Id
    @GeneratedValue(strategy = GenerationType.TABLE)
    private Long entryId;
    private String ipAddress;
    private Date createdOn;
    private String reason;

    public Long getEntryId() {
        return entryId;
    }

    public void setEntryId(Long entryId) {
        this.entryId = entryId;
    }

    public String getIpAddress() {
        return ipAddress;
    }

    public void setIpAddress(String ipAddress) {
        this.ipAddress = ipAddress;
    }

    public Date getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(Date createdOn) {
        this.createdOn = createdOn;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        BlockedIp blockedIp = (BlockedIp) o;

        if (!entryId.equals(blockedIp.entryId)) return false;
        if (!ipAddress.equals(blockedIp.ipAddress)) return false;
        if (!createdOn.equals(blockedIp.createdOn)) return false;
        return reason.equals(blockedIp.reason);
    }

    @Override
    public int hashCode() {
        int result = entryId.hashCode();
        result = 31 * result + ipAddress.hashCode();
        result = 31 * result + createdOn.hashCode();
        result = 31 * result + reason.hashCode();
        return result;
    }
}
