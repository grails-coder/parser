The goal is to write a parser in Java that parses web server access log file, loads the log to MySQL and checks if a given IP makes more than a certain number of requests for the given duration. 

Java
----

(1) Create a java tool that can parse and load the given log file to MySQL. The delimiter of the log file is pipe (|)

(2) The tool takes "startDate", "duration" and "threshold" as command line arguments. "startDate" is of "yyyy-MM-dd.HH:mm:ss" format, "duration" can take only "hourly", "daily" as inputs and "threshold" can be an integer.

(3) This is how the tool works:

    //The tool will find any IPs that made more than 100 requests starting from 2017-01-01.13:00:00 to 2017-01-01.14:00:00 (one hour) and print them to console AND also load them to another MySQL table with comments on why it's blocked.
    java -cp "parser.jar" com.ef.Parser --startDate=2017-01-01.13:00:00 --duration=hourly --threshold=100

    //The tool will find any IPs that made more than 250 requests starting from 2017-01-01.13:00:00 to 2017-01-02.13:00:00 (24 hours) and print them to console AND also load them to another MySQL table with comments on why it's blocked.
	java -cp "parser.jar" com.ef.Parser --startDate=2017-01-01.13:00:00 --duration=daily --threshold=250

SQL
---

(1) Write MySQL query to find IPs that mode more than a certain number of requests for a given time period.

    Ex: Write SQL to find IPs that made more than 100 requests starting from 2017-01-01.13:00:00 to 2017-01-01.14:00:00.

(2) Write MySQL query to find requests made by a given IP.
 	

LOG Format
----------

Example: 2017-10-01 03:56:04.326|174.129.239.67|"GET / HTTP/1.1"|200|"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.91 Safari/537.36"


Deliverables
------------

(1) Java program that can be run from command line
	
    java -cp "parser.jar" com.ef.Parser --accesslog=/path/to/file --startDate=2017-01-01.13:00:00 --duration=hourly --threshold=100 

(2) MySQL schema used for the log data

(3) SQL queries for SQL test	

Considerations
--------------

The application was built on top of Spring and Spring JPA, so the application itself will take care of creating the 
database in case it doesn't exist.

Creating the database
---------------------

Requirements didn't ask for an externalized database configuration, indeed it was created but no extra mechanism 
supports it via CLI, so if there's a need to change the database credentials, it will be necessary to
look for file `Parse/src/main/resources/application.properties`, then change credentials and rebuild the jar file,
for building instructions see below section How to Build.
The application relies on having a database called `parser`. Default connection details are as follows 
(from `application.properties`):

    spring.datasource.url = jdbc:mysql://localhost:3306/Parser
    spring.datasource.username = parser-manager
    spring.datasource.password = parser-password

NOTE: The application does NOT need any ddl statement executed, instead it will take care of DB schema creation.
Provided files under `sql` folder are merely informative on what's created.

MySQL schema
------------

Below is the Domain Model used on MySQL database (I used Domain Driven Design so this is the DB design).
![MySQL Scheme](diagrams/domain-model.png)

How to build
----------
Execute following lines of code to create the jar file
    
    cd Parser
    ./gradlew fatJar

Previous lines created the jar file bundle, in order to execute it, issue:

    cd Parser
    java -cp "build/libs/parser.jar" com.ef.Parser --accesslog=/path/to/file --startDate=2017-01-01.13:00:00 --duration=hourly --threshold=100
    

    
    